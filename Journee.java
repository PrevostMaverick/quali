import java.util.ArrayList;

public class Journee {
    private Style matin;
    private Style soir;
    private int spectateurMatin;
    private int spectateurSoir;
    private List<Auteur> auteursMatin = new ArrayList<Auteur>();
    private List<Auteur> auteursSoir = new ArrayList<Auteur>();
    private List<Integer> scoreMatin = new ArrayList<Integer>();
    private List<Integer> scoreSoir = new ArrayList<Integer>();
    public Journee(Style matin, Style midi, Style soir) {
        this.matin = matin;
        this.midi = midi;
        this.soir = soir;
    }
    public void setMatin(Style style){
        this.matin = style;
    }
    public void setSoir(Style style){
        this.soir = style;
    }
    public Style getMatin(){
        return this.matin;
    }
    public Style getSoir(){
        return this.soir;
    }
    public int getSpectateurMatin(){
        return this.spectateurMatin;
    }
    public int getSpectateurSoir(){
        return this.spectateurSoir;
    }
    public int scoreAuteur(Auteur auteur, Style style){
        if (style == this.matin){
            return auteur.getqualitéStyle(style)*this.getSpectateurMatin();
        }
        if (style == this.soir){
            return auteur.getqualitéStyle(style)*this.getSpectateurSoir();
        }
        return 0;
    }
    public int alea(int mini, int maxi){
        return new Random().nextInt(maxi-mini+1) + mini;
    }
    public void creeEpreuveMatin(){
        Style style = Style.values()[alea(1, 3)];
        int nb = alea(150,2500);
        int choix1 = alea(0,2);
        int choix2 = (choix1 + 1) % 3;
        Auteur auteur1 = Auteur.auteurs().get(choix1);
        Auteur auteur2 = Auteur.auteurs().get(choix2);
        auteursMatin.add(auteur1);
        auteursMatin.add(auteur2);
        int score1 = nb * auteur1.getqualitéStyle(style);
        int score2 = nb * auteur2.getqualitéStyle(style);
        scoreMatin.add(score1);
        scoreMatin.add(score2);
        System.out.println("Epreuve matin : ");
        System.out.println("Style : " + style);
        System.out.println("Auteur1 : " + auteur1);
        System.out.println("Auteur2 : " + auteur2);
        System.out.println("Score1 : " + score1);
        System.out.println("Score2 : " + score2);
    }
    public void creeEpreuveSoir(){
        Style style = Style.values()[alea(1, 3)];
        int nb = alea(150,2500);
        int choix1 = alea(0,2);
        int choix2 = (choix1 + 1) % 3;
        Auteur auteur1 = Auteur.auteurs().get(choix1);
        Auteur auteur2 = Auteur.auteurs().get(choix2);
        auteursSoir.add(auteur1);
        auteursSoir.add(auteur2);
        int score1 = nb * auteur1.getqualitéStyle(style);
        int score2 = nb * auteur2.getqualitéStyle(style);
        scoreSoir.add(score1);
        scoreSoir.add(score2);
        System.out.println("Epreuve soir : ");
        System.out.println("Style : " + style);
        System.out.println("Auteur1 : " + auteur1);
        System.out.println("Auteur2 : " + auteur2);
        System.out.println("Score1 : " + score1);
        System.out.println("Score2 : " + score2);
    }
    //score total
    //toString
}
